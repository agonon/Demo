type triangle = { p1 : Point.point; p2 : Point.point; p3 : Point.point; }
val get_p1 : triangle -> Point.point
val get_p2 : triangle -> Point.point
val get_p3 : triangle -> Point.point
val create_triangle : Point.point -> Point.point -> Point.point -> triangle
val ccw : Point.point -> Point.point -> Point.point -> bool
