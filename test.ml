(* Fichier de test de la première présentation. *)

open Point;;
open Point_set;;
open Maths;;
open Triangle;;
open Triangle_set;;
open Drawing;;
open Graphics;;

print_string("Pour lancer le premier test, appuyez sur entrée.\n");;
read_line();;

print_string("Test numéro 1 sur le module Point.\n\n");;

print_string("Ici, nous créons un point p de coordonné (4. ; 5.)\n");;

let p = Point.create 4. 5.;;

print_string("Les coordonnées de p sont :\n");;
print_string("x=");print_float(Point.get_x p);;
print_string("\ny=");print_float(Point.get_y p);;
print_string("\n");;
print_string("Appuyez sur entrée.\n");;

read_line();;

print_string("Test numéro 2 sur le module Point_set.\n\n");;

print_string("On crée et affiche ici un ensemble de 10 points de coordonnées alétoires dans une fenêtre de 800x600.\n");;

let p_s = Point_set.random 10 800 600;;
Drawing.init_draw();;
Drawing.draw_points p_s;;


print_string("Appuyez sur entrée.\n");;

read_line();;
Graphics.close_graph();;

print_string("Test numéro 3 sur le module Maths.\n");;

let det = Maths.determinant (Maths.create_c_by_c [1.; 1.; 1.; 2.; 3.; 4.; 4.; 9.; 16.] 3);;
print_string("le déterminant vaut: ");print_float det;;
print_newline ();;

print_string("Appuyez sur entrée.\n");;

read_line();;

print_string("Test numéro 4 sur le module Triangle.\n");;

let t = create_triangle (create 0. 0.) (create 1. 0.) (create 1. 1.);;
get_p1 t;;
get_p2 t;;
get_p3 t;;
let sens_t = Triangle.ccw (Point.create 0. 0.) (Point.create 1. 0.) (Point.create 1. 1.);;

if sens_t then print_string "true\n" else print_string "false\n";;

print_string("Appuyez sur entrée.\n");;

read_line();;





print_string("Test numéro 5 sur le module Drawing.\n");;

Drawing.init_draw();;
Drawing.draw_one_triangle (Triangle.create_triangle (Point.create 100. 100.) (Point.create 150. 200.) (Point.create 200. 200.));;

print_string("Appuyez sur entrée.\n");;


print_string("Les tests sont terminés.\nVeuillez appuyez sur entrée pour quitter.\n");;

read_line();;

#quit;;

