type point_set = Point.point list
val empty_ps : unit -> point_set
val is_empty_ps : point_set -> bool
val cons : point_set -> Point.point -> Point.point list
val cdr_ps : point_set -> Point.point list
val car_ps : point_set -> Point.point
val random : int -> int -> int -> point_set
