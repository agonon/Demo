#!/bin/bash

ocamlc -i point.ml > point.mli
ocamlc -c point.mli
ocamlc -c point.ml

read d

ocamlc -i point_set.ml > point_set.mli
ocamlc -c point_set.mli
ocamlc -c point_set.ml

read d

ocamlc -i maths.ml > maths.mli
ocamlc -c maths.mli
ocamlc -c maths.ml

read d

ocamlc -i triangle.ml > triangle.mli
ocamlc -c triangle.mli
ocamlc -c triangle.ml

read d

ocamlc -i triangle_set.ml > triangle_set.mli
ocamlc -c triangle_set.mli
ocamlc -c triangle_set.ml

read d

ocamlc -i drawing.ml > drawing.mli
ocamlc -c drawing.mli
ocamlc -c drawing.ml

